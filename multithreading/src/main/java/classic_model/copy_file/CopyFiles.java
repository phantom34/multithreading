package classic_model.copy_file;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

public class CopyFiles implements Runnable {
    private ArrayList<File> pdfFiles;
    private Path endDirectoryPath;

    public CopyFiles(ArrayList<File> files, Path endDirectoryPath){
        this.pdfFiles = files;
        this.endDirectoryPath = endDirectoryPath;
    }

    @Override
    public void run() {
        System.out.println("Copy start");
        for (File f : pdfFiles) {
            try {
                copyFile(f, endDirectoryPath);
            } catch (Exception e){
                e.getStackTrace();
            }
        }
    }

    private void copyFile(File f, Path endDirectoryPath) throws IOException {
        System.out.println("\nFile: " + f.getName() + "\n\t *Thread: " + Thread.currentThread());
        Files.copy(f.toPath(), endDirectoryPath.resolve(f.getName()), StandardCopyOption.REPLACE_EXISTING);
    }
}

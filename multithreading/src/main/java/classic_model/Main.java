package classic_model;

import classic_model.view_directory.DirectoryTraversal;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {
    public static void main(String[] args){

        File startDirectory = new File("C:\\Users\\Hanna_Zayenka\\Downloads");
        Path endDirectory = Paths.get("C:\\Users\\Hanna_Zayenka\\Desktop\\target");
        if (startDirectory.exists() && startDirectory.isDirectory()) {
            Thread t = new Thread(new DirectoryTraversal(startDirectory, endDirectory));
            t.start();
        }
    }
}

package classic_model.view_directory;

import classic_model.copy_file.CopyFiles;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;

public class DirectoryTraversal implements Runnable{
    private ArrayList<File> pdfFiles = new ArrayList<>();
    private File startDirectory;
    private Path endDirectory;

    @Override
    public void run() {
        System.out.println("DirectoryTraversal start");
        reviewFileSystem(startDirectory);
        Thread copyFiles = new Thread(new CopyFiles(pdfFiles, endDirectory));
        try{
            copyFiles.join();
        } catch (InterruptedException e){
            e.getMessage();
        }
        copyFiles.start();
    }

    public DirectoryTraversal(File startDirectory, Path endDirectory){
        this.startDirectory = startDirectory;
        this.endDirectory = endDirectory;
    }

    private void reviewFileSystem(File f) {

        ArrayList<File> nodesToReview = new ArrayList<>();

        if (f == null) {
            return;
        }
        if (f.isFile()) {
            return;
        }

        File[] files = f.listFiles();

        if (files.length != 0) {

            for (File content : files) {
                if (content.getName().contains(".pdf")) {
                    pdfFiles.add(content);
                }
                if (content.isDirectory()) {
                    nodesToReview.add(content);
                }
            }
        }
        if(!nodesToReview.isEmpty()) {
            for (File nodes : nodesToReview) {
                System.out.println("\nDirectory: " + nodes.getName() + "\n\t\t *Thread: " + Thread.currentThread());
                reviewFileSystem(nodes);
            }
        }
    }
}
